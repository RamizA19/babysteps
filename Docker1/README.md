# Docker!

A quick run through of what docker is and how it can work

We covered two use cases in this 
1. A outward facing api that can be hit and responds with something. We wanted this to be contantly listening
2. To run a script inside a docker container (whether or not the container was alive after was optional)

## Usecase 1
- Rename the file Dockerfile to Dockerfile.newscript
- Rename the file Dockerfile.newapp to Dockerfile
- `docker build -t newappimage .`
- `docker run -it --name newappcontainer newappimage`

## Usecase 2
- Rename the file Dockerfile to Dockerfile.newapp
- Rename the file Dockerfile.newscript to Dockerfile
- `docker build -t newscriptimage .`
- `docker run -it --name newscriptcontainer newscriptimage`

## Docker commands
- Help for each command --- `docker <command> --help`
- `-d` : run the container in the background, dont `exec` into it on `run`
- `--rm` : clear up (remove) the container on stop; part of `docker run`
- `docker ps` : see all alive containers
- `docker ps -a` : see all containers (alive/stopped/killed/exited)
- `docker images` : lists out all the images on your local machine
- `docker rm <containerID>` : remove the container that is still on your machine. Use the `-f` to force it to kill even if running
- `docker rmi <imageID>` : remove the image from your machine

CheatSheet
- `docker rm -f $(docker ps -aq)` : removes all the containers on your machine - dead or alive
- `docker rmi $(docker images | grep "^<none>" | awk "{print $3}")` : remove all the images with the name `<none>` : cleans out all images with `<none>` tag