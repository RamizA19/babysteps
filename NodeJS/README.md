# Writing a simple Node API (with Express)

0. Initiae a new Node project.
After you have created your new folder for this project and have opened a terminal in that folder, enter the below command.
```
# this will give you a prompt to set out the initial elements of the project
npm init

# if you know you will say yes to everything already
npm init -y
```

0. Setup and key components
First thing you need is the below line. This will enable the code to be executed in strict mode which means you can and can't do certain things; like not being able to use undeclared variables. 
```
use strict;
```

0. Packages
Each node project has a npm package manager that will house all the packages you are using. It will cover all the versions that the project is using and thus gives a single place for all your dependencies. The is created when the project is created in the first steps. The `package.json` file is located in the source directory.
Once you have a `package.json` file in your repo, you will need to do two things to use a package in the classes of your project.
- first you need to add a line in the code that uses the keyword `require` followed by the name of the page. 
- secondly you will need to open a terminal window at the root folder and run the command `npm install` followed by the name of the page. Once this is complete, the package name and version will appear in the `package.json` file.

## Exmple Project
This project has an example of this code, and it has been dockerised to show you the minimum files that are needed.

## Important notes
0. The code does not always run line by line. It will attempt to execute the code as it comes. For example, if you have line that requires a certain function to complete first, you need to use a Promises. This will allow you to chain the different functions to execute once the previous one has completed.
0. 