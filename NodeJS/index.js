'use strict';

const express = require('express')
const app = express()

app.listen(3000, function(){
    console.log('the app has started on port 3000')
})

app.get('/test', function(req, res){
    console.log('test endpoint has been hit')
    res.send('yep endpoint is still live')
})