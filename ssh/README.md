# A quick guide to using SSH

## Intro
SSH is like a secure token that comes in two parts, private and public key. These two work to help with authenticate or encrypt data.

Fundementals of public/private key encryption: if one key is used to ENcrypt something, then only the other key can be used to DEcrypt it. For example if you use the public key to ENcrypt a message, then you can only use the private key to DEcrypt it (and vice versa).

### SSH authentication
There is a way to use SSH public/private keys to securely login into machines. 
Theory: if you set it so that there is a password which is encrypted with your public key, then you can login in by providing your private key, decryp the password and enter it to give you access to the machine.

Process:

0. First you need to have a machine that is set up to use SSh encryption as the password method. 
0. Now you need to create a SSH key. Go to the SSH directory (`cd ~/.ssh`) and create a new ssh token (`ssh-keygen`). This will come up with some options you need to populate. Once this is done, you should have 2 new files in your `~/.ssh` folder - one file with the name you choose in the options and another with the same name ending with `.pub`. The file ending with the `.pub` is the public key, the other is the private key.
0. You need to now copy the content in the public key. To do this use this command `cat YOUR-KEY-NAME.pub | pbcopy`. 
0. In the settings for the machine, it will ask for the public key. Paste the content you just copied into there and save it.
0. Your done! Your machine can now use SSH keys to login securely.
